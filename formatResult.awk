BEGIN {ssthresh=0;bytesInFlight=0;enter=0;printf("ssthresh\tbytesInFlight\tcWndAtTheEnd\tTimeSpentInRecovery\tBytesSentInRecovery\n");}
{
if($1 == "[node")
  {
    if($2 == "0]")
      {
        if($3=="Enter:")
          {
            enter = 1;
            ssthresh = $4;
            bytesInFlight = $5
          }
        if($3=="RTODuringRecovery")
          {
            enter = 0;
            printf("%d\t\t%d\t\t%d\t\t%f\t\t%d\n", ssthresh, bytesInFlight, $5, $6, $7)
          }
        if($3=="Exit:" && enter==1)
          {
            printf("%d\t\t%d\t\t%d\t\t%f\t\t%d\n", ssthresh, bytesInFlight, $5, $6, $7)
            enter = 0;
          }
      }
  }
}
END {
}
